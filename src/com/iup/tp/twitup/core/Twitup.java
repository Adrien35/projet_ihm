package com.iup.tp.twitup.core;

import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.UUID;

import com.iup.tp.twitup.common.Constants;
import com.iup.tp.twitup.common.PropertiesManager;
import com.iup.tp.twitup.datamodel.Database;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.events.file.IWatchableDirectory;
import com.iup.tp.twitup.events.file.WatchableDirectory;
import com.iup.tp.twitup.ihm.TwitupMainView;
import com.iup.tp.twitup.ihm.TwitupMock;
import com.iup.tp.twitup.logger.Logger;
import com.iup.tp.twitup.model.SessionManager;

import javax.swing.*;

/**
 * Classe principale l'application.
 *
 * @author S.Lucas
 */
public class Twitup {
	/**
	 * Base de données.
	 */
	protected IDatabase mDatabase;

	/**
	 * Gestionnaire des entités contenu de la base de données.
	 */
	protected EntityManager mEntityManager;

	/**
	 * Gestionnaire des sessions de l'application.
	 */
	protected SessionManager sessionManager;

	/**
	 * Vue principale de l'application.
	 */
	protected TwitupMainView mMainView;

	/**
	 * Classe de surveillance de répertoire
	 */
	protected IWatchableDirectory mWatchableDirectory;

	/**
	 * Répertoire d'échange de l'application.
	 */
	protected String mExchangeDirectoryPath;

	/**
	 * Idnique si le mode bouchoné est activé.
	 */
	protected boolean mIsMockEnabled = false;

	/**
	 * Look&Feel
	 */
	protected String lookandFeel = null;

	/**
	 * Nom de la classe de l'UI.
	 */
	protected String mUiClassName;

	/**
	 * Constructeur.
	 */
	public Twitup() {

		// Initialisation des propriétés
		this.initProperties();

		// Initialisation du look and feel de l'application
		this.initLookAndFeel();

		// Initialisation de la base de données
		this.initDatabase();

		if (this.mIsMockEnabled) {

			// Initialisation du bouchon de travail
			this.initMock();
		}

		// Initialisation de la session
		this.initSession();

		// Initialisation de l'IHM
		this.initGui();

		// Initialisation du répertoire d'échange
		this.initDirectory();

		// Initialisation des samples d'users et twits
		this.initUsersandTwits();
	}

	/**
	 * Initialisation des propriétés à l'aide du fichier de configuration
	 */
	protected void initProperties() {

		Logger.info("Initialisation des propriétés");

		Properties appProps = PropertiesManager.loadProperties(Constants.CONFIGURATION_FILE);

		this.mExchangeDirectoryPath =  "" + appProps.getProperty(Constants.CONFIGURATION_KEY_EXCHANGE_DIRECTORY);
		this.mIsMockEnabled = Boolean.valueOf(appProps.getProperty(Constants.CONFIGURATION_KEY_MOCK_ENABLED));
		this.mUiClassName =  "" + appProps.getProperty(Constants.CONFIGURATION_KEY_UI_CLASS_NAME);
		this.lookandFeel =  "" + appProps.getProperty(Constants.CONFIGURATION_KEY_LOOK_FEEL);
	}

	/**
	 * Initialisation du look and feel de l'application.
	 */
	protected void initLookAndFeel() {

		Logger.info("Initialisation du Look & Feel");
		try {
			if(this.lookandFeel.equals("metal")){
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} else if(this.lookandFeel.equals("windows")) {

			} else if(this.lookandFeel.equals("mac")) {

			} else if(this.lookandFeel.equals("default")) {
				UIManager.setLookAndFeel(UIManager.getLookAndFeel());
			}else {
				UIManager.setLookAndFeel(this.lookandFeel);
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			Logger.error("Look & Feel spécifié inconnu : application du thème par défaut");
			try {
				//L&F de la plateforme qui utilise l'application
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (InstantiationException e1) {
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				e1.printStackTrace();
			} catch (UnsupportedLookAndFeelException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Initialisation de l'interface graphique.
	 */
	protected void initGui() {

		Logger.info("Initialisation de l'interface graphique");

		this.mMainView = new TwitupMainView(this);
	}

	/**
	 * Initialisation du répertoire d'échange (depuis la conf ou depuis un file
	 * chooser). <br/>
	 * <b>Le chemin doit obligatoirement avoir été saisi et être valide avant de
	 * pouvoir utiliser l'application</b>
	 */
	protected void initDirectory() {

		if(this.isValideExchangeDirectory(new File(this.mExchangeDirectoryPath))) {

			this.initDirectory(this.mExchangeDirectoryPath);
		} else {

			Logger.info("Initialisation du répertoire d'échange");
			Logger.error("Aucun répertoire d'échange courant valide défini");
		}
	}

	/**
	 * Indique si le fichier donné est valide pour servire de répertoire
	 * d'échange
	 *
	 * @param directory
	 *            , Répertoire à tester.
	 */
	protected boolean isValideExchangeDirectory(File directory) {

		// Valide si répertoire disponible en lecture et écriture
		return directory != null && directory.exists() && directory.isDirectory() && directory.canRead()
				&& directory.canWrite();
	}

	/**
	 * Initialisation du mode bouchoné de l'application
	 */
	protected void initMock() {

		Logger.info("Initialisation du mode bouchoné");

		TwitupMock mock = new TwitupMock(this.mDatabase, this.mEntityManager);
		mock.showGUI();
	}

	/**
	 * Initialisation de la base de données
	 */
	protected void initDatabase() {

		Logger.info("Initialisation de la base de données");

		this.mDatabase = new Database();
		this.mEntityManager = new EntityManager(mDatabase);
	}

	/**
	 * Initialisation de la session
	 */
	protected void initSession() {

		Logger.info("Initialisation de la session");

		this.sessionManager = new SessionManager(this.mEntityManager);
	}

	/**
	 * Initialisation du répertoire d'échange.
	 *
	 * @param directoryPath
	 */
	public void initDirectory(String directoryPath) {

		Logger.info("Initialisation du répertoire d'échange");

		if(this.isValideExchangeDirectory(new File(directoryPath))) {

			Logger.info("Répertoire d'échange courant : " + directoryPath);

			this.mExchangeDirectoryPath = directoryPath;
			this.mWatchableDirectory = new WatchableDirectory(directoryPath);
			this.mEntityManager.setExchangeDirectory(directoryPath);

			this.mWatchableDirectory.initWatching();
			this.mWatchableDirectory.addObserver(mEntityManager);
		} else {

			Logger.error("Aucun répertoire d'échange courant valide défini");
		}
	}

	/**
	 * Initialisation des users et des twits
	 */
	public void initUsersandTwits() {
		//user1
		User user1 = new User(UUID.randomUUID(), "ellils", "a",
				"Adrien LE BRETON", new HashSet<String>(), getClass().getResource("/images/adrien.jpg").toString());
		Twit user1_twit1 = new Twit(user1, "Salut les fréros !! #laTeam");
		Twit user1_twit2 = new Twit(user1, "Comment ca va? #caFarte");
		Twit user1_twit3 = new Twit(user1, "Vauban ce week-end ? #laPicole");
		Twit user1_twit4 = new Twit(user1, "@qxzjy dans le dur ce matin");
		Twit user1_twit5 = new Twit(user1, "Vends des iPads #lowcost");

		//user2
		User user2 = new User(UUID.randomUUID(), "qxzjy", "a",
				"Maxime RENAULT", new HashSet<String>(), getClass().getResource("/images/maxime.png").toString());
		Twit user2_twit1 = new Twit(user2, "Ca bosse dur les projets !! #laTeam");
		Twit user2_twit2 = new Twit(user2, "Voici les informations pour la soirée");
		Twit user2_twit3 = new Twit(user2, "Ca allume tout #vauban");
		Twit user2_twit4 = new Twit(user2, "@ellils dur aussi fréro");
		Twit user2_twit5 = new Twit(user2, "@ellils pourquoi tu vends des iPads??");

		User user3 = new User(UUID.randomUUID(), "laGrandeMasse", "a",
				"Clément IGNACIO", new HashSet<String>(), getClass().getResource("/images/masse.jpg").toString());
		Twit user3_twit1 = new Twit(user3,"Compte CO2 en force");
		Twit user3_twit2 = new Twit(user3, "Je m'entraine pour les semis #rude");
		Twit user3_twit3 = new Twit(user3, "#laTeam j'arrete de boire");
		Twit user3_twit4 = new Twit(user3, "Réveil à 6h du matin pour m'entrainer");
		Twit user3_twit5 = new Twit(user3, "@ellils je suis interessey");

		User user4 = new User(UUID.randomUUID(), "BigRolls", "a",
				"Roland CASTEL", new HashSet<String>(), getClass().getResource("/images/ricard.png").toString());
		Twit user4_twit1 = new Twit(user4, "Ca va super bien ! La grande forme");
		Twit user4_twit2 = new Twit(user4, "One jeudi soir ?");
		Twit user4_twit3 = new Twit(user4, "Chaud pour un double jaune @laTeam");
		Twit user4_twit4 = new Twit(user4, "@laGrandeMasse viens picoler avec nous");
		Twit user4_twit5 = new Twit(user4, "Un double jaune #sansglaçons");

		User user5 = new User(UUID.randomUUID(), "guigui29", "a",
				"Guillaume LAMANDA", new HashSet<String>(), getClass().getResource("/images/roue.jpg").toString());
		Twit user5_twit1 = new Twit(user5, "#OnePlus5");
		Twit user5_twit2 = new Twit(user5, "Impeccable la team");
		Twit user5_twit3 = new Twit(user5, "J'apprends Twitup");
		Twit user5_twit4 = new Twit(user5, "#plusdethunes");
		Twit user5_twit5 = new Twit(user5, "@BigRolls et sans eau mon grand");

		this.mDatabase.addUser(user1);
		this.mDatabase.addTwit(user1_twit1);
		this.mDatabase.addTwit(user1_twit2);
		this.mDatabase.addTwit(user1_twit3);
		this.mDatabase.addTwit(user1_twit4);
		this.mDatabase.addTwit(user1_twit5);

		this.mDatabase.addUser(user2);
		this.mDatabase.addTwit(user2_twit1);
		this.mDatabase.addTwit(user2_twit2);
		this.mDatabase.addTwit(user2_twit3);
		this.mDatabase.addTwit(user2_twit4);
		this.mDatabase.addTwit(user2_twit5);

		this.mDatabase.addUser(user3);
		this.mDatabase.addTwit(user3_twit1);
		this.mDatabase.addTwit(user3_twit2);
		this.mDatabase.addTwit(user3_twit3);
		this.mDatabase.addTwit(user3_twit4);
		this.mDatabase.addTwit(user3_twit5);

		this.mDatabase.addUser(user4);
		this.mDatabase.addTwit(user4_twit1);
		this.mDatabase.addTwit(user4_twit2);
		this.mDatabase.addTwit(user4_twit3);
		this.mDatabase.addTwit(user4_twit4);
		this.mDatabase.addTwit(user4_twit5);

		this.mDatabase.addUser(user5);
		this.mDatabase.addTwit(user5_twit1);
		this.mDatabase.addTwit(user5_twit2);
		this.mDatabase.addTwit(user5_twit3);
		this.mDatabase.addTwit(user5_twit4);
		this.mDatabase.addTwit(user5_twit5);
	}

	public EntityManager getmEntityManager() {

		return mEntityManager;
	}

	public SessionManager getSessionManager() {

		return sessionManager;
	}
}
