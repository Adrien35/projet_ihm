package com.iup.tp.twitup.common;

import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class LanguageManager {

    private static LanguageManager ourInstance = new LanguageManager();
    private ResourceBundle bundle;
    protected Properties properties = PropertiesManager.loadProperties(Constants.CONFIGURATION_FILE);


    public static LanguageManager getInstance() {
        return ourInstance;
    }

    private LanguageManager() {
         bundle = ResourceBundle.getBundle("lang", new Locale(properties.getProperty(Constants.CONFIGURATION_KEY_LANGUAGE), properties.getProperty(Constants.CONFIGURATION_KEY_LANGUAGE).toUpperCase()));
    }

    public ResourceBundle getBundle() {
        return bundle;
    }
}
