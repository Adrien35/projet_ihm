package com.iup.tp.twitup.common;

import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FieldsUtils {

	protected static ResourceBundle bundle = LanguageManager.getInstance().getBundle();

	public static boolean validateField(JTextArea f) {

	  if (f.getText().equals("") || f.getText().length() > 150){

	    return false;
	  } else {

		  return true;
	  }

	}

	public static boolean validateField(JTextField f) {

	  if ( f.getText().equals("")){

	    return false;
	  } else {

		  return true;
	  }

	}

	public static boolean validateField(JPasswordField f) {

	  if ( f.getPassword().length == 0){

	    return false;
	  } else {

		  return true;
	  }

	}

	public static boolean validateIntegerField(JTextField f) {

		try {

			int i = Integer.parseInt(f.getText());

			if ( i > 0 ) {

			  return true;
			}
		} catch(Exception e) { }

		return false;
	}

	public static void failedMessage(String errormsg) {

	  JOptionPane.showMessageDialog(null, errormsg, bundle.getObject("CHAMPSINVALIDESTITRE").toString(), JOptionPane.WARNING_MESSAGE);
	}
}
