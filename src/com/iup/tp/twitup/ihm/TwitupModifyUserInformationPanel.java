package com.iup.tp.twitup.ihm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.iup.tp.twitup.common.FieldsUtils;
import com.iup.tp.twitup.common.LanguageManager;
import com.iup.tp.twitup.datamodel.User;

public class TwitupModifyUserInformationPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected TwitupImagePanel twitupImagePanel;
	
	protected JButton modifyIconButton;
	
	protected JTextField nameTextfield;
	
	protected TwitupMainView twitupMainView;
	
	protected User currentUser;
	
	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();

	public TwitupModifyUserInformationPanel(TwitupMainView twitupMainView) {
		
		setLayout(new GridBagLayout());
		
		this.twitupMainView = twitupMainView;
		
		this.twitupImagePanel = new TwitupImagePanel();
		
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		
		this.currentUser = twitupMainView.getController().getSessionManager().getCurrentUser();
		
		this.modifyIconButton = new JButton(bundle.getObject("MODIFIER").toString()); 
		
		this.loadOrRefreshAvatar();
		
		JLabel nameLabel = new JLabel(bundle.getObject("NOM").toString() + " : ");
		
		this.nameTextfield = new JTextField(currentUser.getName());
		this.nameTextfield.setPreferredSize(new Dimension(screenSize.width/6, screenSize.height/25));
		
		JButton okButton = new JButton(bundle.getObject("VALIDER").toString());
		
		JButton returnButton = new JButton();
		Image img;
		try {
			img = ImageIO.read( getClass().getResource("/images/exitIcon_20.png"));
			returnButton.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.modifyIconButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupModifyUserInformationPanel.this.modifyClicked();
			}
		});
		
		
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupModifyUserInformationPanel.this.okClicked();
			}
		});
		
		returnButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupModifyUserInformationPanel.this.returnClicked();
			}
		});
		
		add(new JLabel("@" + currentUser.getUserTag()), new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(this.twitupImagePanel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(this.modifyIconButton, new GridBagConstraints(0, 3, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(nameLabel, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(nameTextfield, new GridBagConstraints(2, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		add(okButton, new GridBagConstraints(2, 4, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(returnButton, new GridBagConstraints(2, 0, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
	}

	protected void modifyClicked() {
		
		JFileChooser fileChooser = new JFileChooser();

        int option = fileChooser.showOpenDialog(twitupMainView.mFrame);

        if(option == JFileChooser.APPROVE_OPTION){

        	this.twitupMainView.getController().getSessionManager().getCurrentUser().setAvatarPath("file:" + fileChooser.getSelectedFile().getAbsolutePath());
        	this.loadOrRefreshAvatar();
        }
	}

	protected void okClicked() {
		
		if(validateFields()) {
			
			this.twitupMainView.getController().getSessionManager().getCurrentUser().setName(this.nameTextfield.getText());
			this.twitupMainView.getTwitupView().initPanelWithUser();
		}
	}
	
	protected void returnClicked() {
		
		this.twitupMainView.getTwitupView().initPanelWithUser();
	}
	
	protected boolean validateFields() {

		if (!FieldsUtils.validateField(this.nameTextfield)) {

			FieldsUtils.failedMessage(bundle.getObject("CHAMPSINVALIDES").toString());
			return false;
		} 
		
		return true;
	}
	
	protected void invalidInformationsDialog() {
		
		  JOptionPane.showMessageDialog(null, bundle.getObject("MAUVAISESINFORMATIONS").toString(), bundle.getObject("MAUVAISESINFORMATIONSTITRE").toString(), JOptionPane.WARNING_MESSAGE); 
	}
	
	protected void loadOrRefreshAvatar() {
		
		this.twitupImagePanel.removeAll();
		this.twitupImagePanel.validate();
		
		try {

			this.twitupImagePanel = new TwitupImagePanel(new URL(this.twitupMainView.getController().getSessionManager().getCurrentUser().getAvatarPath()));
			add(this.twitupImagePanel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
					GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.twitupImagePanel.setPreferredSize(new Dimension(50, 50));
		
		this.twitupImagePanel.revalidate();
    	this.twitupImagePanel.repaint();
	}
}
