package com.iup.tp.twitup.ihm;

import javax.swing.*;

import com.iup.tp.twitup.datamodel.Twit;

import java.awt.*;

public class TwitupShowTwitPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("deprecation")
	public TwitupShowTwitPanel(Twit twit) {

		setLayout(new GridBagLayout());

		JLabel nameLabel = new JLabel(twit.getTwiter().getName() + " (@" + twit.getTwiter().getUserTag() + ")");
		nameLabel.setPreferredSize(new Dimension(250, 20));
		JTextArea nameTextfield = new JTextArea(twit.getText());
		nameTextfield.setPreferredSize(new Dimension(270, 40));
		nameTextfield.disable();
		add(nameLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		add(nameTextfield, new GridBagConstraints(1, 0, 1, 1, 3, 1, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

	}
}
