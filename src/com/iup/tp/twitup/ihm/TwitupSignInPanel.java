package com.iup.tp.twitup.ihm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.iup.tp.twitup.common.FieldsUtils;
import com.iup.tp.twitup.common.LanguageManager;
import com.iup.tp.twitup.datamodel.User;

public class TwitupSignInPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();
	
	protected TwitupMainView twitupMainView;
	
	protected JTextField tagTextfield;
	
	protected JPasswordField passwordTextfield;

	public TwitupSignInPanel(TwitupMainView twitupMainView) {

		setLayout(new GridBagLayout());

		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		
		this.twitupMainView = twitupMainView;

		JLabel tagLabel = new JLabel(bundle.getObject("TAG").toString() + " * : ");
		
		this.tagTextfield = new JTextField();
		this.tagTextfield.setPreferredSize(new Dimension(screenSize.width/6, screenSize.height/25));
		
		JLabel passwordLabel = new JLabel(bundle.getObject("MOTDEPASSE").toString() + " * : ");
		
		this.passwordTextfield = new JPasswordField();
		this.passwordTextfield.setPreferredSize(new Dimension(screenSize.width/6, screenSize.height/25));
		
		JLabel requiredFieldsLabel = new JLabel("(*) " + bundle.getObject("CHAMPSOBLIGATOIRES").toString());
		
		JButton okButton = new JButton(bundle.getObject("VALIDER").toString());
		
		JButton returnButton = new JButton(bundle.getObject("RETOUR").toString());
		
		add(tagLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(tagTextfield, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		
		add(passwordLabel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(passwordTextfield, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

		add(requiredFieldsLabel, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupSignInPanel.this.okClicked();
			}
		});
		
		returnButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupSignInPanel.this.returnClicked();
			}
		});
		
		add(okButton, new GridBagConstraints(1, 3, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
		add(returnButton, new GridBagConstraints(0, 3, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
	}

	
	protected void okClicked() {
		
		if(validateFields()) {
			
			Set<User> users = this.twitupMainView.getController().getmEntityManager().getDatabase().getUsers();
			User userToConnect = null;
			Boolean wrongPassword = true;
			
			for(User user : users) {
				
				if(user.getUserTag().equals(this.tagTextfield.getText())) {
					
					if(user.getUserPassword().equals(String.valueOf(this.passwordTextfield.getPassword()))) {
						
						wrongPassword = false;
						userToConnect = user;
					}
				}
			}
			
			if(wrongPassword) {
				
				this.invalidInformationsDialog();
			} else {
				
				this.twitupMainView.getController().getmEntityManager().getDatabase().addUser(userToConnect);
				this.twitupMainView.getController().getSessionManager().setCurrentUser(userToConnect);
				this.twitupMainView.getTwitupView().initPanelWithUser();
				this.twitupMainView.getTwitupMenuBar().getDisconnectButton().setEnabled(true);
			}
		}
	}
	
	protected void returnClicked() {
		
		this.twitupMainView.getTwitupView().changePanel(new TwitupSignInRegisterPanel(this.twitupMainView));
	}
	
	protected boolean validateFields() {

		if (!FieldsUtils.validateField(this.tagTextfield) || !FieldsUtils.validateField(this.passwordTextfield)) {

			FieldsUtils.failedMessage(bundle.getObject("CHAMPSINVALIDES").toString());
			return false;
		} 
		
		return true;
	}
	
	protected void invalidInformationsDialog() {
		
		  JOptionPane.showMessageDialog(null, bundle.getObject("MAUVAISESINFORMATIONS").toString(), bundle.getObject("MAUVAISESINFORMATIONSTITRE").toString(), JOptionPane.WARNING_MESSAGE); 
	}
}
