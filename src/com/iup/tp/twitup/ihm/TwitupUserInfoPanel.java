package com.iup.tp.twitup.ihm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.iup.tp.twitup.common.LanguageManager;
import com.iup.tp.twitup.datamodel.User;

public class TwitupUserInfoPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected JLabel userInfo;
	
	protected JButton modifyInformationsButton;
	
	protected JButton searchUserButton;
	
	protected JButton searchTwitButton;
	
	protected TwitupImagePanel twitupImagePanel;
	
	protected TwitupMainView twitupMainView;
	
	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();

	public TwitupUserInfoPanel(TwitupMainView twitupMainView) {
		
		setLayout(new GridBagLayout());
		
		this.twitupMainView = twitupMainView;
		
		User user = twitupMainView.getController().getSessionManager().getCurrentUser();
		
		this.modifyInformationsButton = new JButton();
		
		this.searchUserButton = new JButton();
		
		this.searchTwitButton = new JButton();
		
		try {
			this.twitupImagePanel = new TwitupImagePanel(new URL(user.getAvatarPath()));
			
			Image img = ImageIO.read( getClass().getResource("/images/hashtag.png"));
			Image img1 = ImageIO.read( getClass().getResource("/images/user.png"));
			Image img2 = ImageIO.read( getClass().getResource("/images/settings.png"));
			
			this.modifyInformationsButton.setIcon(new ImageIcon(img2));
			this.searchUserButton.setIcon(new ImageIcon(img1));
			this.searchTwitButton.setIcon(new ImageIcon(img));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.twitupImagePanel.setPreferredSize(new Dimension(50, 50));
		
		this.userInfo = new JLabel(user.getName() + " | @" + user.getUserTag() + " | " + user.getFollows().size() + " " + bundle.getObject("ABONNE").toString());
		
		this.modifyInformationsButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupUserInfoPanel.this.modifyClicked();
			}
		});
		
		this.searchUserButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupUserInfoPanel.this.searchUserClicked();
			}
		});
		
		this.searchTwitButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupUserInfoPanel.this.searchTwitClicked();
			}
		});
		
		add(this.searchTwitButton, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(this.searchUserButton, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(this.modifyInformationsButton, new GridBagConstraints(2, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(this.userInfo, new GridBagConstraints(3, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(this.twitupImagePanel, new GridBagConstraints(4, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
	}
	
	protected void modifyClicked() {
		
		this.twitupMainView.getTwitupView().changePanel(new TwitupModifyUserInformationPanel(this.twitupMainView));
	}
	
	protected void searchUserClicked() {
		
		this.twitupMainView.getTwitupView().changePanel(new TwitupSearchUserPanel(this.twitupMainView));
	}
	
	protected void searchTwitClicked() {
		
		this.twitupMainView.getTwitupView().changePanel(new TwitupSearchTwitPanel(this.twitupMainView));
	}
}
