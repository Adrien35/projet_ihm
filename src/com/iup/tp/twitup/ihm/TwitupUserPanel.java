package com.iup.tp.twitup.ihm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.iup.tp.twitup.common.LanguageManager;
import com.iup.tp.twitup.datamodel.User;

public class TwitupUserPanel extends JPanel{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	protected TwitupMainView twitupMainView;

	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();

	public TwitupUserPanel(User user) {

		setLayout(new GridBagLayout());

		JLabel userLabel = new JLabel(user.getName() + " | @" + user.getUserTag() + " | " + user.getFollows().size() + " " + bundle.getObject("ABONNE").toString());
		add(userLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		setPreferredSize(new Dimension(350, 30));
	}
}
