package com.iup.tp.twitup.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import com.iup.tp.twitup.core.Twitup;

/**
 * Classe de la vue principale de l'application.
 */
public class TwitupMainView {

	/**
	 * Fenetre principale
	 */
	protected JFrame mFrame;

	protected TwitupMenuBar menuBar;

	protected TwitupView twitupView;

	/**
	 * Controlleur principal
	 */
	protected Twitup controller;

	public TwitupMainView(Twitup controller) {

		this.controller = controller;
		this.showGUI();
	}

	/**
	 * Lance l'afficahge de l'IHM.
	 */
	public void showGUI() {

		if (mFrame == null) {
			this.initGUI();
		}

		// Affichage dans l'EDT
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// Custom de l'affichage
				JFrame frame = TwitupMainView.this.mFrame;
				Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
				frame.setLocation((screenSize.width - frame.getWidth()) / 6,
						(screenSize.height - frame.getHeight()) / 6);

				// Affichage
				TwitupMainView.this.mFrame.setResizable(false);
				TwitupMainView.this.mFrame.setVisible(true);
				TwitupMainView.this.mFrame.pack();
			}
		});
	}

	/**
	 * Initialisation de l'IHM
	 */
	protected void initGUI() {

		// Création de la fenetre principale
		mFrame = new JFrame("TwitUp");
		mFrame.setLayout(new BorderLayout());

		//menuBar
        this.menuBar = new TwitupMenuBar(this);
		this.mFrame.add(menuBar, BorderLayout.NORTH);

        //View
        this.twitupView = new TwitupView(this);

		this.mFrame.add(twitupView, BorderLayout.CENTER);
	}

	protected TwitupView getTwitupView() {

		return this.twitupView;
	}

	protected TwitupMenuBar getTwitupMenuBar() {

		return this.menuBar;
	}

	protected JFrame getFrame() {

		return this.mFrame;
	}

	protected Twitup getController() {

		return this.controller;
	}
}
