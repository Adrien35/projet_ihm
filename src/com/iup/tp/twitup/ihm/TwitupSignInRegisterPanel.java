package com.iup.tp.twitup.ihm;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.iup.tp.twitup.common.LanguageManager;

public class TwitupSignInRegisterPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();
	
	protected TwitupMainView twitupMainView;
	
	public TwitupSignInRegisterPanel(TwitupMainView twitupMainView) {
		
		setLayout(new GridBagLayout());
		
		this.twitupMainView = twitupMainView;
		
		JLabel requiredFieldsLabel = new JLabel(bundle.getObject("BIENVENUE").toString());
		JButton signInButton = new JButton(bundle.getObject("INSCRIPTION").toString());
		JButton connectionButton = new JButton(bundle.getObject("CONNEXION").toString());
				
		signInButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupSignInRegisterPanel.this.openRegisterPanel();
			}
		});
		
		
		connectionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupSignInRegisterPanel.this.openSignInPanel();
			}
		});
		
		add(requiredFieldsLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(signInButton, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(connectionButton, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
	}
	
	protected void openSignInPanel() {
		
		this.twitupMainView.getTwitupView().changePanel(new TwitupSignInPanel(this.twitupMainView));
	}
	
	protected void openRegisterPanel() {
		
		this.twitupMainView.getTwitupView().changePanel(new TwitupRegisterPanel(this.twitupMainView));
	}
}
