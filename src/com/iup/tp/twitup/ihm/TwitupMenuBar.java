package com.iup.tp.twitup.ihm;

import com.iup.tp.twitup.common.LanguageManager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class TwitupMenuBar extends JMenuBar{

	protected TwitupMainView twitupMainView;

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();
	
	protected JMenu fileMenu;
	
	protected JMenuItem openMenuItem;
	
	protected JMenuItem disconnectMenuItem;
	
	protected JMenuItem quitMenuItem;
	
	protected ImageIcon closeIcon;
	
	protected JMenu helpMenu;
	
	protected JMenuItem aboutMenuItem;
	
	protected ImageIcon aboutIcon;

	public TwitupMenuBar(TwitupMainView twitupMainView) {

		this.twitupMainView = twitupMainView;
		
		this.aboutIcon = new ImageIcon(getClass().getResource("/images/logoIUP_50.jpg"));


		this.fileMenu = new JMenu(bundle.getObject("FICHIER").toString());

		this.openMenuItem = new JMenuItem(bundle.getObject("OUVRIR").toString());
		
		this.disconnectMenuItem = new JMenuItem(bundle.getObject("DECONNEXION").toString());
		this.disconnectMenuItem.setEnabled(false);
		

		this.quitMenuItem = new JMenuItem(bundle.getObject("QUITTER").toString());
		this.closeIcon = new ImageIcon(getClass().getResource("/images/exitIcon_20.png"));
		this.quitMenuItem.setIcon(closeIcon);
		

		this.helpMenu = new JMenu(bundle.getObject("AIDE").toString());
		this.aboutMenuItem = new JMenuItem(bundle.getObject("A_PROPOS").toString());
		
		this.openMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupMenuBar.this.chooseDirectory();
			}
		});
		
		this.disconnectMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupMenuBar.this.disconnectUser();
			}
		});
		
		this.quitMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupMenuBar.this.quitApplication();
			}
		});
		
		this.aboutMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupMenuBar.this.aboutMenu();
			}
		});

		this.fileMenu.add(openMenuItem);
		this.fileMenu.add(disconnectMenuItem);
		this.fileMenu.add(quitMenuItem);

		this.helpMenu.add(aboutMenuItem);

		add(fileMenu);
		add(helpMenu);
	}

	/**
	 * Choisir un dossier
	 */
	public void chooseDirectory() {

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int option = fileChooser.showOpenDialog(twitupMainView.mFrame);

        if(option == JFileChooser.APPROVE_OPTION){

        	this.twitupMainView.getController().initDirectory(fileChooser.getSelectedFile().getAbsolutePath());
        }
	}
	
	/**
	 * Deconnecter un utilisateur
	 */
	protected void disconnectUser() {
		
		// Enlever utilisateur courant
		this.twitupMainView.getTwitupView().initPanel();
		this.disconnectMenuItem.setEnabled(false);
	}

	/**
	 * Quitter l'application
	 */
	protected void quitApplication() {

		System.exit(0);
	}

	/**
	 * Menu À propos
	 */
	protected void aboutMenu() {

		JOptionPane.showMessageDialog(
				twitupMainView.mFrame,
				bundle.getObject("FORMATION").toString()+"\n"+bundle.getObject("DEPARTEMENT").toString(),
				bundle.getObject("A_PROPOS").toString(),
                JOptionPane.WARNING_MESSAGE, aboutIcon);
	}
	
	public JMenuItem getDisconnectButton() {
		
		return disconnectMenuItem;
	}
}
