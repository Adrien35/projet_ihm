package com.iup.tp.twitup.ihm;

import javax.swing.*;

import com.iup.tp.twitup.common.FieldsUtils;
import com.iup.tp.twitup.common.LanguageManager;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.datamodel.User;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

public class TwitupCreateTwitPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	protected TwitupMainView twitupMainView;

	protected JLabel twitLabel;

	protected JTextArea twitTextArea;

	protected JButton twitButton;
	
	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();

	public TwitupCreateTwitPanel(TwitupMainView twitupMainView) {

		setLayout(new GridBagLayout());
		
		this.twitupMainView = twitupMainView;

		this.twitLabel = new JLabel(bundle.getObject("NOUVEAUTWIT").toString() + " : ");
		this.twitTextArea = new JTextArea();
		this.twitButton = new JButton(bundle.getObject("POSTER").toString());
		
		this.twitButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupCreateTwitPanel.this.twitClicked();
			}
		});

		add(this.twitLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.VERTICAL, new Insets(5, 5, 5, 5), 0, 0));

		this.twitTextArea.setPreferredSize(new Dimension(270, 40));
		add(this.twitTextArea, new GridBagConstraints(0, 1, 1, 1, 4, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		

		add(this.twitButton, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

	}
	
	protected void twitClicked() {
		
		if(validateFields()) {
			
			User user = this.twitupMainView.getController().getSessionManager().getCurrentUser();
			Twit twit = new Twit(user, this.twitTextArea.getText());
			
			this.twitupMainView.getController().getmEntityManager().getDatabase().addTwit(twit);
			this.twitupMainView.getTwitupView().loadOrRefreshMainPanel();
			this.twitTextArea.setText("");
		}
	}
	
	protected boolean validateFields() {

		if (!FieldsUtils.validateField(this.twitTextArea)) {

			FieldsUtils.failedMessage(bundle.getObject("CHAMPSINVALIDES").toString());
			return false;
		} 
		
		return true;
	}
}
