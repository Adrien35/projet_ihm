package com.iup.tp.twitup.ihm;

import javax.swing.*;

import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.datamodel.User;

import java.awt.*;
import java.util.ArrayList;

public class TwitupView extends JPanel {

	private static final long serialVersionUID = 1L;

	protected TwitupCreateTwitPanel twitupCreateTwitPanel;

	protected TwitupImagePanel twitupImagePanel;

	protected JPanel mainPanel;

	protected JLabel userInfo;

	protected JScrollPane scrollPane;

	protected TwitupMainView twitupMainView;

	protected TwitupUserInfoPanel twitupUserInfoPanel;

	public TwitupView(TwitupMainView twitupMainView) {

		this.twitupMainView = twitupMainView;

		this.mainPanel = new JPanel();
		this.mainPanel.setLayout(new GridBagLayout());

		setLayout(new GridBagLayout());

		initPanel();
	}

	protected void initPanel() {

		this.removeAll();
		this.validate();

		add(new TwitupSignInRegisterPanel(twitupMainView), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

		this.revalidate();
    	this.repaint();

    	this.twitupMainView.getFrame().pack();
	}

	protected void initPanelWithUser() {

		this.removeAll();
		this.validate();



		this.twitupUserInfoPanel = new TwitupUserInfoPanel(twitupMainView);

		this.twitupCreateTwitPanel = new TwitupCreateTwitPanel(twitupMainView);

		this.loadOrRefreshMainPanel();

		this.scrollPane = new JScrollPane(this.mainPanel);
		this.scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.scrollPane.setBounds(0, 0, 570, 250);
        this.scrollPane.setPreferredSize(new Dimension(570, 250));

        // Panel info utilisateur
 		add(this.twitupUserInfoPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
 				GridBagConstraints.VERTICAL, new Insets(5, 5, 5, 5), 0, 0));

		// Panel principal
		add(this.scrollPane, new GridBagConstraints(0, 1, 2, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

		// Panel création twit
		add(this.twitupCreateTwitPanel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

    	this.revalidate();
    	this.repaint();

    	this.twitupMainView.getFrame().pack();
	}

	protected void changePanel(JPanel jpanel) {

		this.removeAll();
		this.validate();

		add(jpanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

		this.revalidate();
    	this.repaint();

    	this.twitupMainView.getFrame().pack();
	}

	protected void loadOrRefreshMainPanel() {

		this.mainPanel.removeAll();
		this.mainPanel.validate();

		User user = this.twitupMainView.getController().getSessionManager().getCurrentUser();

		ArrayList<Twit> twits = new ArrayList<Twit>(this.twitupMainView.getController().getmEntityManager().getDatabase().getUserTwits(user));

		for(int i=0; i < twits.size(); i++) {

			this.mainPanel.add(new TwitupShowTwitPanel(twits.get(i)), new GridBagConstraints(0, i, 1, 1, 1, 1, GridBagConstraints.CENTER,
					GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		}

		this.mainPanel.revalidate();
    	this.mainPanel.repaint();
	}
}
