package com.iup.tp.twitup.ihm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.iup.tp.twitup.common.FieldsUtils;
import com.iup.tp.twitup.common.LanguageManager;
import com.iup.tp.twitup.datamodel.User;

public class TwitupRegisterPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected TwitupMainView twitupMainView;
	
	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();
	
	protected JTextField nameTextfield;
	
	protected JTextField tagTextfield;
	
	protected JPasswordField passwordTextfield;

	public TwitupRegisterPanel(TwitupMainView twitupMainView) {
		
		setLayout(new GridBagLayout());
		
		this.twitupMainView = twitupMainView;
		
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		
		JLabel nameLabel = new JLabel(bundle.getObject("NOM").toString() + " * : ");
		
		this.nameTextfield = new JTextField();
		this.nameTextfield.setPreferredSize(new Dimension(screenSize.width/6, screenSize.height/25));
		
		JLabel tagLabel = new JLabel(bundle.getObject("TAG").toString() + " * : ");
		
		this.tagTextfield = new JTextField();
		this.tagTextfield.setPreferredSize(new Dimension(screenSize.width/6, screenSize.height/25));
		
		JLabel passwordLabel = new JLabel(bundle.getObject("MOTDEPASSE").toString() + " * : ");
		
		this.passwordTextfield = new JPasswordField();
		this.passwordTextfield.setPreferredSize(new Dimension(screenSize.width/6, screenSize.height/25));
		
		JLabel requiredFieldsLabel = new JLabel("(*) " + bundle.getObject("CHAMPSOBLIGATOIRES").toString());
		
		JButton okButton = new JButton(bundle.getObject("VALIDER").toString());
		
		JButton returnButton = new JButton(bundle.getObject("RETOUR").toString());
		
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupRegisterPanel.this.okClicked();
			}
		});
		
		returnButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupRegisterPanel.this.returnClicked();
			}
		});
		
		add(nameLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(nameTextfield, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		
		add(tagLabel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(tagTextfield, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		
		
		add(passwordLabel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(passwordTextfield, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.EAST,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		
		
		add(requiredFieldsLabel, new GridBagConstraints(1, 3, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
		
		add(okButton, new GridBagConstraints(1, 4, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
		add(returnButton, new GridBagConstraints(0, 4, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
	}
	
	protected void okClicked() {
		
		if(validateFields()) {
			
			Set<User> users = this.twitupMainView.getController().getmEntityManager().getDatabase().getUsers();
			Boolean userAlreadyExist = false;
			
			for(User user : users) {
				
				if(user.getUserTag().equals(this.tagTextfield.getText())) {
					
					userAlreadyExist = true;
				}
			}
			
			if(userAlreadyExist) {
				
				this.userAlreadyExistDialog();
			} else {
				
				User user = new User(UUID.randomUUID(), this.tagTextfield.getText(), String.valueOf(this.passwordTextfield.getPassword()), 
						 			 this.nameTextfield.getText(), new HashSet<String>(), getClass().getResource("/images/logo.png").toString());
				this.twitupMainView.getController().getmEntityManager().getDatabase().addUser(user);
				this.twitupMainView.getController().getSessionManager().setCurrentUser(user);
				this.twitupMainView.getTwitupView().initPanelWithUser();
				this.twitupMainView.getTwitupMenuBar().getDisconnectButton().setEnabled(true);
			}
		}
	}
	
	protected void returnClicked() {
		
		this.twitupMainView.getTwitupView().changePanel(new TwitupSignInRegisterPanel(this.twitupMainView));
	}
	
	protected boolean validateFields() {

		if (!FieldsUtils.validateField(this.nameTextfield) || !FieldsUtils.validateField(this.tagTextfield) || !FieldsUtils.validateField(this.passwordTextfield)) {

			FieldsUtils.failedMessage(bundle.getObject("CHAMPSINVALIDES").toString());
			return false;
		} 
		
		return true;
	}
	
	protected void userAlreadyExistDialog() {
		
		  JOptionPane.showMessageDialog(null, bundle.getObject("UTILISATEUREXISTE").toString(), bundle.getObject("UTILISATEUREXISTETITRE").toString(), JOptionPane.WARNING_MESSAGE); 
	}
}
