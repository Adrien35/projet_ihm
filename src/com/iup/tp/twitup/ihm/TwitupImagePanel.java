package com.iup.tp.twitup.ihm;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

public class TwitupImagePanel extends JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Image image = null;
	
	public TwitupImagePanel() {
		
	}

    public TwitupImagePanel(URL img) {
    	
        try {
        	
            this.image = ImageIO.read(img);
        } catch (IOException e) {
        	
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g) {
    	
        super.paintComponent(g);
        
        if (image != null) {
        	
            int height = this.getSize().height;
            int width = this.getSize().width;
            g.drawImage(image, 0, 0, width, height, this);
        }
    }

}
