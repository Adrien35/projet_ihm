package com.iup.tp.twitup.ihm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.iup.tp.twitup.common.LanguageManager;
import com.iup.tp.twitup.datamodel.User;

public class TwitupSearchUserPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected TwitupMainView twitupMainView;
	
	protected JButton returnButton;
	
	protected JButton searchButton;
	
	protected JTextField searchField;
	
	protected JPanel mainPanel;
	
	protected JScrollPane scrollPane;
	
	protected ResourceBundle bundle = LanguageManager.getInstance().getBundle();
	
	public TwitupSearchUserPanel(TwitupMainView twitupMainView) {
		
		this.twitupMainView = twitupMainView;
		
		this.mainPanel = new JPanel();
		this.mainPanel.setLayout(new GridBagLayout());
		
		setLayout(new GridBagLayout());
		
		this.searchField = new JTextField();
		
		this.searchButton = new JButton();
		
		this.returnButton = new JButton();
		
		try {
			
			Image img = ImageIO.read( getClass().getResource("/images/search.png"));
			Image img1 = ImageIO.read( getClass().getResource("/images/left-arrow.png"));
			
			this.searchButton.setIcon(new ImageIcon(img));
			this.returnButton.setIcon(new ImageIcon(img1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.returnButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupSearchUserPanel.this.returnClicked();
			}
		});
		
		this.searchButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				TwitupSearchUserPanel.this.searchClicked();
			}
		});
		
		this.loadOrRefreshMainPanel(false);
		
		this.scrollPane = new JScrollPane(this.mainPanel);		
		this.scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.scrollPane.setBounds(0, 0, 350, 250);
        this.scrollPane.setPreferredSize(new Dimension(350, 250));
		
		add(this.returnButton, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(this.searchButton, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(this.searchField, new GridBagConstraints(2, 0, 1, 1, 20, 1, GridBagConstraints.WEST,
						GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		add(this.scrollPane, new GridBagConstraints(0, 1, 3, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
	}
	
	protected void returnClicked() {
		
		this.twitupMainView.getTwitupView().initPanelWithUser();
	}
	
	protected void searchClicked() {
		
		this.loadOrRefreshMainPanel(true);
		this.searchField.setText("");
	}
	
	protected void loadOrRefreshMainPanel(Boolean filter) {
		
		this.mainPanel.removeAll();
		this.mainPanel.validate();
		
		ArrayList<User> users;

		if(filter && !this.searchField.getText().equals("")) {
			
			users = this.searchUserContains();
		} else {
			
			users = new ArrayList<User>(this.twitupMainView.getController().getmEntityManager().getDatabase().getUsers());
		}
		
		for(int i=0; i < users.size(); i++) {
			
			this.mainPanel.add(new TwitupUserPanel(users.get(i)), new GridBagConstraints(0, i, 1, 1, 1, 1, GridBagConstraints.CENTER,
					GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		}
		
		this.mainPanel.revalidate();
    	this.mainPanel.repaint();
	}
	
	protected ArrayList<User> searchUserContains(){
		
		ArrayList<User> users = new ArrayList<User>();
		
		for(User user: this.twitupMainView.getController().getmEntityManager().getDatabase().getUsers()) {
			
			if(user.getName().toLowerCase().indexOf(this.searchField.getText().toLowerCase()) != -1 || 
			   user.getUserTag().toLowerCase().indexOf(this.searchField.getText().toLowerCase()) != -1) {
				
				users.add(user);
			} 
		}
		
		return users;
	}
}
