package com.iup.tp.twitup.logger;

public class Logger {	
	
    public static void info(String log) {           
        
    	System.out.print("Info > " +log + "\n");
    }
    
    public static void error(String log) {           
        
    	System.err.print("Error > " + log  + "\n");
    }
}
