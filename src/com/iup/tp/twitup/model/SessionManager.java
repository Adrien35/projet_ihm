package com.iup.tp.twitup.model;

import java.util.Set;

import com.iup.tp.twitup.core.EntityManager;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.datamodel.User;

public class SessionManager {

	protected User currentUser;
	
	protected Set<Twit> twitSet;
	
	protected EntityManager entityManager;

	public SessionManager(EntityManager entityManager) {
		
		this.entityManager = entityManager;
		this.currentUser = null;
		this.twitSet = null;		
	}

	public User getCurrentUser() {
		
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		
		this.currentUser = currentUser;
		

	}

	public Set<Twit> getTwitSet() {
		
		return twitSet;
	}

	public void setTwitSet(Set<Twit> twitSet) {
		
		this.twitSet = twitSet;
	}

	public EntityManager getEntityManager() {
		
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}
}